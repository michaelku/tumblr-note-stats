$(document).ready(function(){

	//Hide E-mail Body Text
	$('#most').fadeOut();
	$('#top').fadeOut();

	$('#display_all').bind('click', function(e){
		e.preventDefault;
		$('.minimal_frame').fadeOut();
		$('#all').fadeIn();

      	$('.display_option').css('background-color', '#6f6f6f');
      	$(this).parent().css('background-color', '#5f5f5f');
	});

	$('#display_most').bind('click', function(e){
		e.preventDefault;
		$('.minimal_frame').fadeOut();
		$('#most').fadeIn();

      	$('.display_option').css('background-color', '#6f6f6f');
      	$(this).parent().css('background-color', '#5f5f5f');
	});

	$('#display_top').bind('click', function(e){
		e.preventDefault;
		$('.minimal_frame').fadeOut();
		$('#top').fadeIn();

      	$('.display_option').css('background-color', '#6f6f6f');
      	$(this).parent().css('background-color', '#5f5f5f');
	});
});