<?php
	//include("notes.php");

	/*
	if(isset($_POST['submit'])){
		if(isset($_POST['notes'])){
			$notes = $_POST['notes'];
			$notes = preg_replace( '/\s+/', ' ', $notes);
		}
	}
	*/	

	//http://stackoverflow.com/questions/9153224/how-to-limit-file-upload-type-file-size-in-php

/*************************/
/* GET File was Uploaded */
/*************************/

	if(isset($_FILES['file'])){

		//Navigation (replace)
		print "
			<ul>
				<li><a href = '#all'>All Notes</a></li>
				<li><a href = '#posters'>Top Posters</a></li>
				<li><a href = '#sources'>Top Sources</a></li>
			</ul>
		";

		//Max filesize = 5MB and only allows .TXT and .HTML files
		$errors = array();
		$max_size = 524288000;
		$file_types = array(
			'text/plain',
			'text/html'
		);

		$file_size = $_FILES['file']['size'];
   		$file_type = $_FILES['file']['type'];

   		//Checks file size
   		if(($file_size >= $max_size) || ($file_size == 0)) {
        	$errors[] = 'File too large. File must be less than 5 megabytes.';
   		}

   		//Checks file types
   		if((!in_array($file_type, $file_types)) && (!empty($file_type))) {
        	$errors[] = 'Invalid file type. Only .TXT and .HTML types are accepted.';
    	}

    	//If no errors, continue
    	if(count($errors) == 0) {
        	$path = $_FILES["file"]["tmp_name"];
			$notes = file_get_contents("$path", true);
		}

		//Else, if errors, die.
		else {
		    foreach($errors as $error) {
		        echo '<script>alert("'.$error.'");</script>';
		    }

		    die(); //Ensure no more processing is done
		}

		//Explodes notes by ending list tag for a count
		//Does not count the last item (ghost item)
		$by_note = explode("</li>", $notes, -1);

		//Not sure if I need this but whatever
		$note_count = count($by_note);

		//Start table
		print "
			<a name = 'all'></a> 
			<table border = 1 cellpadding = 5>
				<tr>
					<td colspan = 2>Notes Log</td>
					<td colspan = 2>Total Notes: $note_count</td>
				</tr>
		";

		//Note count needed for looping later on
		$i = $note_count;

		//Array of note elements
		$poster_array = array();
		$source_array = array();
		$more_array = array();

/**********************/
/* Begin note parsing */
/**********************/

		//Loop through notes
		foreach($by_note as $singular_note){

			$note = explode('class="action"', $singular_note);

			//Scan note for these keywords
			//Not sure if it will break if these words are used in text format
			$remainder = $note[1];
			$reblog = "/reblogged/";
			$like = "/likes/";
			$replied = "/said/";
			$posted = "/posted/";

			$if_reblog = preg_match($reblog, $remainder);
			$if_like = preg_match($like, $remainder);
			$if_replied = preg_match($replied, $remainder);
			$if_posted = preg_match($posted, $remainder);

			//If REBLOG
			if($if_reblog == true){
				//Get Permalink
				$link = explode('data-post-url="', $note[1]);
				$link = explode('">', $link[1]);
				$link = $link[0];

				//Get Poster Name
				$note = explode('href="http://', $note[1]);
				$poster = explode('.tumblr.com/', $note[1]);
				$poster = $poster[0];

				//Custom Poster?
				$custom_poster = explode('/', $poster);
				$poster = $custom_poster[0];

				//Poster Array Push
				array_push($poster_array, $poster);

				//Get Reblog Source
				$source = explode('.tumblr.com/', $note[2]);
				$source = $source[0];

				//Custom Source?
				$custom_source = explode('/', $source);
				$source = $custom_source[0];

				//Source Array Push
				array_push($source_array, $source);
				
				//Check to see if there is added text.
				if(isset($note[3])){
					$more = explode('">', $note[3]);
					$more = $more[1];
				}
				else{
					$more = "none";
				}

				//More Array Push
				array_push($more_array, $more);

				print "
					<tr>
						<td>$i</td>
						<td><a href = 'http://$poster.tumblr.com/'>$poster</a></td>
						<td><a href = '$link'>reblogged</a></td>
						<td><a href = 'http://$source.tumblr.com/'>$source</a></td>
				";

				if($more !== "none"){
					print "<td>$more</td>";
				}

				print "</tr>";

			}

			else if($if_replied == true){
				//Get Poster Name
				$note = explode('href="http://', $note[1]);
				$poster = explode('.tumblr.com/', $note[1]);
				$poster = $poster[0];

				//Custom Poster?
				$custom_poster = explode('/', $poster);
				$poster = $custom_poster[0];

				//Poster Array Push
				array_push($poster_array, $poster);
				
				$reply_text = explode('<span class="answer_content">', $note[1]);
				$reply_text = explode('</span>', $reply_text[1]);
				$more = $reply_text[0];

				//More Array Push
				array_push($more_array, $more);

				print "
					<tr>
						<td>$i</td>
						<td><a href = 'http://$poster.tumblr.com/'>$poster</a></td>
						<td><a href = '$link'>replied</a></td>
						<td>with</td>
				";

				if($more !== "none"){
					print "<td>$more</td>";
				}

				print "</tr>";

			}

			//If LIKE or POSTED (no additional information)
			else{
				$note = explode('href="http://', $note[1]);
				$poster = explode('.tumblr.com/', $note[1]);
				$poster = $poster[0];

				//Custom Poster?
				$custom_poster = explode('/', $poster);
				$poster = $custom_poster[0];

				//Poster Array Push
				array_push($poster_array, $poster);

				if($if_like == true){
					$message = "liked";
				}

				else if($if_posted == true){
					$message = "posted";
				}

				print "
					<tr>
						<td>$i</td>
						<td><a href = 'http://$poster.tumblr.com/'>$poster</a></td>
						<td>$message</td>
					</tr>
				";
			}

			//$i++;
			$i--;;;
		}
		print "</table> <br />";

		//Posters Table
		$posters = array_count_values($poster_array);
		arsort($posters);

		print "
			<a name = 'posters'></a> 
			<table border = 1 cellpadding = 5>
				<tr>
					<td colspan = 2>Top Posters</td>
				</tr>
		";

		foreach($posters as $poster_info => $values){
			print "
				<tr>
					<td><a href = 'http://$poster_info.tumblr.com/'>$poster_info</a></td>
					<td>$values</td>

				</tr>
			";
		}

		print "</table> <br />";

		//Sources Table
		$sources = array_count_values($source_array);
		arsort($sources);
		
		print "
			<a name = 'sources'></a> 
			<table border = 1 cellpadding = 5>
				<tr>
					<td colspan = 2>Top Sources</td>
				</tr>
		";

		foreach($sources as $source_info => $values){
			print "
				<tr>
					<td><a href = 'http://$source_info.tumblr.com/'>$source_info</a></td>
					<td>$values</td>

				</tr>
			";
		}

		print "</table>";
	}
?>