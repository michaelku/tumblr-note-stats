<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Tumblr Note Stats - Michael Ku</title>
		<!--<link rel="stylesheet" type="text/css" href="css/styles.css">-->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
		<script type="text/javascript" src = "js/form.js"></script>
	</head>
	<body>
		<h1>Tumblr Note Stats</h1>
		<form id = "uploadForm" action = "stats.php" method = "post" enctype="multipart/form-data">
			<input type = "file" name = "file" id = "file"><br />
			<input type = "submit" name = "submit" id = "notes_submit" value = "Get Stats!" />
		</form>
		<p>
			How to Use:
			<ol>
				<li>Manually load all notes for a post by going to the posts permalink and spamming the fuck out of "Show more Notes"</li>
				<li>In Google Chrome, right-click and use the "Inspect Element" tool, and using CTRL+F, search for <code>&lt;ol class="notes"&gt;</code></li>
				<li>In Firefox, do the same but when the toolbar comes up, click the second button from the left, find <code>&lt;ol class="notes"&gt;</code>, and choose the "Copy Outer HTML" option</li>
				<li>Right-click the node and click the "Copy as HTML" option</li>
				<li>Paste the code in Notepad a similar program and save as a .TXT file</li>
				<li>Upload file via form above</li>
			</ol>
		</p>
		<p>Too lazy to make this page pretty.<br />
		Created by <a href = "http://michaelku.com">Michael Ku</a></p>
	</body>
</html>